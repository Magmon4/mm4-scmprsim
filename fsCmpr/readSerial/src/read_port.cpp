#include "UARTIFace.hpp"

int
main( int    argc,
      char** argv  )
{
    UARTIFace UARTIFaceObj;
    DataBuffer readBuf;
    UARTIFaceObj.readPort(readBuf);
    for ( auto i = readBuf.begin(); i != readBuf.end(); ++i )
        std::cout << * i << " ";

    return EXIT_SUCCESS ;
}
