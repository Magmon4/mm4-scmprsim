Build Instructions:
1- Build TinyFrame (Customized) and install in PC 
	$ cd libTinyFrame
	$ mkdir build
	$ cd build
	$ cmake ..
	$ make
	$ sudo make install

2- Build f50S PC Simulator
	$ cd projectDir
	$ mkdir build
	$ cd build
	$ cmake ..
	$ make

Testing:
	@ cd projectDir/build // Should have two executable : 1- mm4 , 2- fsComp
	@ Open Terminal 1:
		@ Execute fsComp 
			$ ./fsComp
	@ Open Terminal 2:
		@ Execute mm4
			$ ./mm4
