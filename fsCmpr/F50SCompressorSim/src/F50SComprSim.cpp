/**
 * @brief 
 * 
 * @file F50SComprSim.cpp
 * @author Vipin Kumar
 * @date 2018-08-07
 */

#include "F50SComprSim.hpp"

void F50SComprSim::rxThread () {
    int n = 0;
    std::cout << "In Thread!!" << std::endl;
    while(!mKillRxThread) {
        printf("\nWaiting for magmon...\n");
        //Clear the buffer
        recvDataBuff.clear();
        UARTIFaceSingleton::Instance()->waitUntilDataAvailable();
        UARTIFaceSingleton::Instance()->readPort(recvDataBuff);
        n = recvDataBuff.size();
        printf("\033[36m--- RX %ld bytes ---\033[0m\n", n);
        //dumpFrame(&recvDataBuff[0], n);
        TF_Accept(F50SCompr_tf, &recvDataBuff[0], (size_t) n);
    }
}
 
void F50SComprSim::dataSimThread () {
    while(!mKillDataSimThread) {
        auto nextTimePoint = std::chrono::steady_clock::now() + tickInterval();

        std::cout << "In DataSim Thread\n";
        simulateComprSData();

        if (nextTimePoint < std::chrono::steady_clock::now()) {
            fprintf(stderr, "Query commands taking more time than QUERY INTERVAL\n");
        }
        else {
            std::this_thread::sleep_until(nextTimePoint);
        }
    }
}
TF_Result MM4Listener(TinyFrame *tf, TF_Msg *msg)
{
    char buff[RESP_BUFF_SIZE];
    printf("MM4Listener()\n");
    dumpFrameInfo(msg);
    F50SComprSingleton::Instance()->handleRxPacket(tf, msg);
    return TF_STAY;
}

void F50SComprSim::Initialization(void) {
    F50SCompr_tf = TF_Init(TF_SLAVE);
    TF_AddGenericListener(F50SCompr_tf, MM4Listener);
}

void F50SComprSim::simulateComprSData(void) {
    setAllTemp();
    setAllPres();
    setCurrStatusCode();
    setPresAlarmInfoBegin();
    pF50SData.softVersion = SOFT_VERSION;
}

void F50SComprSim::handleRxPacket(TinyFrame *tf, TF_Msg * msg) {
    //switch Cases
    string data = (char *)msg->data;
    vector<string> token = splitData(data, dataDelim);
    string cmd = token[0];
    if(cmd.compare(sCmprCmdStr[AllTempVal]) == 0)
        respondAllTemp(tf, msg);
    else if(cmd.compare(sCmprCmdStr[HeTempPreH20]) == 0)
        respondHeTempPreH2O(tf, msg);
    else if(cmd.compare(sCmprCmdStr[HeTempAftH20]) == 0)
        respondHeTempAftH2O(tf, msg);
    else if(cmd.compare(sCmprCmdStr[OilTempPreH20]) == 0)
        respondOilTempPreH2O(tf, msg);
    else if(cmd.compare(sCmprCmdStr[OilTempAftH20]) == 0)
        respondOilTempAftH2O(tf, msg);
    else if(cmd.compare(sCmprCmdStr[WaterTempInlet]) == 0)
        respondWaterTempInlet(tf, msg);
    else if(cmd.compare(sCmprCmdStr[WaterTempOutlet]) == 0)
        respondWaterTempOutlet(tf, msg);
    else if(cmd.compare(sCmprCmdStr[AllPresVal]) == 0)
        respondAllPres(tf, msg);
    else if(cmd.compare(sCmprCmdStr[CurStatusCode]) == 0)
        respondCurStatusCode(tf, msg);
    else if(cmd.compare(sCmprCmdStr[OnSCmpr]) == 0)
        respondOnSCmpr(tf, msg);
    else if(cmd.compare(sCmprCmdStr[OffSCmpr]) == 0)
        respondOffSCmpr(tf, msg);
    else if(cmd.compare(sCmprCmdStr[PresAlarmInfoBegin]) == 0)
        respondPresAlarmInfoBegin(tf, msg);
    else if(cmd.compare(sCmprCmdStr[SoftVersion]) == 0)
        respondSoftVersion(tf, msg);
    
}

void F50SComprSim::respondAllTemp(TinyFrame *tf, TF_Msg * msg) {
    static int tempCount = 0;
    tempCount++;
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,%6.1f,%6.1f,%6.1f,%6.1f,%6.1f,",
    msg->data, getHeTempPreH20() + tempCount, getHeTempAftH20()+ tempCount, getOilTempPreH20()+ tempCount,
     getOilTempAftH20()+ tempCount, getWaterTempInlet()+ tempCount, getWaterTempOutlet()+ tempCount);
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondHeTempPreH2O(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getHeTempPreH20());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondHeTempAftH2O(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getHeTempAftH20());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondOilTempPreH2O(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getOilTempPreH20());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondOilTempAftH2O(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getOilTempAftH20());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondWaterTempInlet(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getWaterTempInlet());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondWaterTempOutlet(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,", msg->data, getWaterTempOutlet());   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondAllPres(TinyFrame *tf, TF_Msg * msg) {
    static int presCount = 0;
    presCount++;
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%6.1f,%6.1f,",
    msg->data, getReturnPressure() + presCount, getSupplyPressure()+ presCount);
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondCurStatusCode(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%s,", msg->data, getCurrStatusCode().c_str());
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondOnSCmpr(TinyFrame *tf, TF_Msg * msg) {
    pF50SData.powerStatus = true;
    //! Logic to start Compressor
    static char buff[RESP_BUFF_SIZE];
    if(getCmprPowerStatus())
        sprintf(buff, "%s,", msg->data);   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondOffSCmpr(TinyFrame *tf, TF_Msg * msg) {
    pF50SData.powerStatus = false;
    //! Logic to stop Compressor
    static char buff[RESP_BUFF_SIZE];
    if(!getCmprPowerStatus())
        sprintf(buff, "%s,", msg->data);   
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondPresAlarmInfoBegin(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,", msg->data,
    pF50SData.alarmInfoBegin[0].c_str(), pF50SData.alarmInfoBegin[1].c_str(), pF50SData.alarmInfoBegin[2].c_str(),
    pF50SData.alarmInfoBegin[3].c_str(), pF50SData.alarmInfoBegin[4].c_str(), pF50SData.alarmInfoBegin[5].c_str(),
    pF50SData.alarmInfoBegin[6].c_str(), pF50SData.alarmInfoBegin[7].c_str(), pF50SData.alarmInfoBegin[8].c_str(),
    pF50SData.alarmInfoBegin[9].c_str());
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondColdHeadFreq(std::vector <std::string> &token, TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    changeColdHeadFreq(stof(token[1])); // Index 1 contains the data (freq)
    sprintf(buff, "%s,%6.1f,", msg->data, getColdHeadFreq());
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::respondSoftVersion(TinyFrame *tf, TF_Msg * msg) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,%s,", msg->data, pF50SData.softVersion.c_str());
    msg->data = (const uint8_t *) buff;
    msg->len = (TF_LEN) strlen((const char *) msg->data);
    TF_Respond(tf, msg);
}

void F50SComprSim::changeColdHeadFreq(float coldHeadFreq) {
    pF50SData.currentCHOprFreq = coldHeadFreq;
    // Compressor action to change coldhead frequency
}

void F50SComprSim::setAllTemp(void) {
    for(auto i = 0; i < TEMP_MAX; i++) {
        pF50SData.temperature[i] = Random::get(-999.9, 999.9);
    }
}

void F50SComprSim::setAllPres() {
    for(auto i = 0; i < PRESSURE_MAX; i++) {
        pF50SData.pressure[i] = Random::get(-9.99, 9.99);
    }
}

void F50SComprSim::setCurrStatusCode(void) {
    auto statusCodeIndex = Random::get(0, (int)Max_Status - 1);
    pF50SData.currentStatusCode = comprSStatusCodeStr[(ComprSStatusCode)statusCodeIndex];
}

void F50SComprSim::setPresAlarmInfoBegin(void) {
    std::string alarmCode;
    std::set <std::string> randomAlarmCode;
    for(auto i = 0; i < MAX_ALARM_FROM_BEGIN; i++) {
        do {
            alarmCode = * Random::get(comprSAlarmCode);
        } while(!randomAlarmCode.insert(alarmCode).second);
        pF50SData.alarmInfoBegin[i] = alarmCode;
    }
}

bool F50SComprSim::getCmprPowerStatus(void) {
    return pF50SData.powerStatus;
} 

std::string F50SComprSim::getCurrStatusCode(void) {
    return pF50SData.currentStatusCode;
}

vector <string> splitData(const string &data, char delim) { 
    vector <string> tokens;
    stringstream check1(data);
     
    string intermediate;
     
    while(getline(check1, intermediate, delim))
    {
        tokens.push_back(intermediate);
    }
    return tokens;
 }

/**
 * Demo WriteImpl - send stuff to magmon4
 *
 * @param buff
 * @param len
 */
void TF_WriteImpl(TinyFrame *tf, const uint8_t *buff, uint32_t len)
{
    printf("\033[32mTF_WriteImpl - sending frame:\033[0m\n");
    // dumpFrame(buff, len);
    // TBD: IsConnected();
    try{
        UARTIFaceSingleton::Instance()->writePort(buff, len);
    } catch (...) {
        std::cout << "UARTIFace::writePort cached an exception " << std::endl;
    }
}

// helper func for testing
void dumpFrame(const uint8_t *buff, size_t len)
{
    size_t i;
    for (i = 0; i < len; i++) {
        printf("%3u \033[94m%02X\033[0m", buff[i], buff[i]);
        if (buff[i] >= 0x20 && buff[i] < 127) {
            printf(" %c", buff[i]);
        }
        else {
            printf(" \033[31m.\033[0m");
        }
        printf("\n");
    }
    printf("--- end of frame ---\n\n");
}

void dumpFrameInfo(TF_Msg *msg)
{
    printf("\033[33mFrame info\n"
               "  type: %02Xh\n"
               "  data: \"%.*s\"\n"
               "   len: %u\n"
               "    id: %Xh\033[0m\n\n",
           msg->type, msg->len, msg->data, msg->len, msg->frame_id);
}

bool TF_ClaimTx(TinyFrame *tf)
{
    // take mutex
    return true; // we succeeded
}

/** Free the TX interface after composing and sending a frame */
void TF_ReleaseTx(TinyFrame *tf)
{
    // release mutex
}