/*****************************************************************************
****  confManager.cpp
******************************************************************************
Copyright 2017 General Electric Company.  All rights reserved.


Date       By                SPR# & Remarks
---------  ----------------  ------------------------------------------------
                             Original for magmon4
26-03-2018 Vipin Kumar       Added a method to write configuration to cnf file 
******************************************************************************/

#include "confManager.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <list>

const char * OS_RELEASE_FILE = "/etc/os-release";

// trim leading white-spaces
static std::string& ltrim(std::string& s) {
    size_t startpos = s.find_first_not_of(" \t\r\n\v\f\"");
    if (std::string::npos != startpos)
    {
        s = s.substr(startpos);
    }
    return s;
}

// trim trailing white-spaces
static std::string& rtrim(std::string& s) {
    size_t endpos = s.find_last_not_of(" \t\r\n\v\f\"");
    if (std::string::npos != endpos)
    {
        s = s.substr(0, endpos + 1);
    }
    return s;
}

config::config(void) {

}

config::config(const std::string& filename) {
    mfilename = filename;
    parse(filename);
}

/*****************************************************************************
****  get_section
******************************************************************************
   This class member function returns the pointer to the structure having 
   'sectionname'. Using this return pointer we can access the key value pair
   associated with 'sectioname'.

PARAMETERS:
        sectionname
            Name of the section
RETURNS:
        section structure pointer


******************************************************************************/
section* config::get_section(const std::string& sectionname) {
    std::list<section>::iterator found = std::find_if(sections.begin(), sections.end(), [sectionname](const section& sect) {
        return sect.name.compare(sectionname) == 0; });

    return found != sections.end() ? &*found : NULL;
}

std::list<section>& config::get_sections() {
    return sections;
}

/*****************************************************************************
****  writeConfFile
******************************************************************************
   This class member function write the data values of list of 'section' structures
    to the configuration file.

PARAMETERS:
        void   
RETURNS:
        void
******************************************************************************/
void config::writeConfFile(void) {
    std::ofstream ofstrm(mfilename);
    for (auto sec : sections) {
        ofstrm << "[" << sec.name << "]" << std::endl;
            
        std::unordered_map<std::string, std::string>::const_iterator it;
        for (it = sec.keyvalues.begin(); it != sec.keyvalues.end(); it++) {
            ofstrm << it->first << " = " << it->second << std::endl;
        }
           
        ofstrm << std::endl;
    }
}

/*****************************************************************************
****  set_value
******************************************************************************
   This class member function writes the value of the given key and sectionname
   to the structure and then into configuration file.

PARAMETERS:
    sectionname:
        Name of the section.
    keyname:
        Name of the key
    value:
        Value of the key   
RETURNS:
    void
******************************************************************************/
void config::set_value(const std::string& sectionname, const std::string&keyname,
    const std::string& value) {
        std::cout << "set called\n";
    section* sect = get_section(sectionname);
    if (sect != NULL) {
        sect->keyvalues[keyname] = value;
        std::cout << "Write Called\n";
        writeConfFile();
    }
}

/*****************************************************************************
****  get_value
******************************************************************************
   This class member function return the value for a given sectionname and its key.

PARAMETERS:
    sectionname:
        Name of the section.
    keyname:
        Name of the key    
RETURNS:
    value of the requested key.
******************************************************************************/

std::string config::get_value(const std::string& sectionname, const std::string& keyname) {
    section* sect = get_section(sectionname);
    if (sect != NULL) {
        std::unordered_map<std::string, std::string>::const_iterator it = sect->keyvalues.find(keyname);
        if (it != sect->keyvalues.end())
            return it->second;
    }
    return "";
}

/*****************************************************************************
****  get_os_release
******************************************************************************
   This class member function return the value for a given keyname from 
   /etc/os-release file.

PARAMETERS:
    keyname:
        Name of the key.
RETURNS:
    value:
        value of the requested key.
******************************************************************************/
std::string config::get_os_release(const std::string& keyname) {
    std::ifstream fstrm;
    fstrm.open(OS_RELEASE_FILE);
    std::string name;
    std::string value;
    for (std::string line; std::getline(fstrm, line);) {
     size_t end = line.find_first_of("=");
            if (end != std::string::npos) {
                if(keyname.compare(line.substr(0, end)) == 0) {
                    value = line.substr(end + 1);
                    break;
                }
            }
    }
    ltrim(rtrim(value));
    return value;
}

/*****************************************************************************
****  parse
******************************************************************************
   This class member function parse the configuration sections into a structure 
   containing name of section and unordered map of key value pairs.

PARAMETERS:
    filename:
        filename of configuration file to be parse.
RETURNS:
    void
******************************************************************************/
void config::parse(const std::string& filename) {
    section currentsection;
    std::ifstream fstrm;
    fstrm.open(filename);

    if (!fstrm)
        throw std::invalid_argument(filename + " could not be opened");

    for (std::string line; std::getline(fstrm, line);)
    {
        // if a comment
        if (!line.empty() && (line[0] == ';' || line[0] == '#')) {
            // allow both ; and # comments at the start of a line

        }
        else if (line[0] == '[') {
            /* A "[section]" line */
            size_t end = line.find_first_of(']');
            if (end != std::string::npos) {

                // this is a new section so if we have a current section populated, add it to list
                if (!currentsection.name.empty()) {
                    sections.push_back(currentsection);  // copy
                    currentsection.name.clear();  // clear section for re-use
                    currentsection.keyvalues.clear();
                }
                currentsection.name = line.substr(1, end - 1);
            }
            else {
                // section has no closing ] char
            }
        }
        else if (!line.empty()) {
            /* Not a comment, must be a name[=:]value pair */
            size_t end = line.find_first_of("=:");
            if (end != std::string::npos) {
                std::string name = line.substr(0, end);
                std::string value = line.substr(end + 1);
                ltrim(rtrim(name));
                ltrim(rtrim(value));

                currentsection.keyvalues[name] = value;

        }
            else {
                // no key value delimitter
            }
        }
    } // for


    // if we are out of loop we add last section
    // this is a new section so if we have a current section populated, add it to list
    if (!currentsection.name.empty()) {
        sections.push_back(currentsection);  // copy
        currentsection.name = "";
        currentsection.keyvalues.clear();
    }
}