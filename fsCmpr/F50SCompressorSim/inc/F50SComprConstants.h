/**
 * @brief 
 * 
 * @file sComprEnums.h
 * @author your name
 * @date 2018-09-05
 */
#ifndef SCOMPRENUMS_H_
#define SCOMPRENUMS_H_

const char dataDelim = ',';

typedef enum {
    INTERNAL = 0,
    DSUB_15,
    RS232,
} F50SComprMode;

typedef enum {
    eCmprSHeTempPreH20,
    eCmprSHeTempAftH20,
    eCmprSOilTempPreH20,
    eCmprSOilTempAftH20,
    eCmprsSWaterTempInlet,
    eCmprsSWaterTempOutlet
} CmprSTemp;

typedef enum {
    eCmprSHePresSupply,
    eCmprSHePresReturn
} CmprSPres;

typedef enum {
    BeforeInit,
    Init,
    StopByInitByAlarm,
    Stop,
    PrepareRun,
    PrepareCHOnlyRun,
    Run,
    CHOnlyRun,
    PrepareStop,
    PrepareCHOnlyStop,
    FaultOffByAlarm,
    AlarmReset,
    Max_Status
} ComprSStatusCode;

typedef enum {
    AllTempVal ,
    HeTempPreH20, HeTempAftH20, OilTempPreH20, OilTempAftH20, WaterTempInlet, WaterTempOutlet,
    AllPresVal,
    HePressureSupply, HePressureReturn,
    TotalRunTime, AdsorberUsageTime, AdsorberRemainderTime,
    SoftVersion, SysStructFileClass, AbnormalStopHistory, WarnHistory,
    OnSCmpr, OffSCmpr, ResetAlarm, OnIndCHOpr, OffIndCHOpr,
    CurrCHOprFreq, SetCHOptFreq, ServiceTimer, CurStatusCode,
    PresAlarmInfoBegin, PresAlarmInfoCont,
    FailurePred1, FailurePred2, FailurePred3, FailurePred4, FailurePred5,
    maxCmd
} sCmprCmd;


#endif //!SCOMPRENUMS_H_
