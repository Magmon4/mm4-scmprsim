#ifndef CONFIG_HPP_
#define CONFIG_HPP_

/*****************************************************************************
****   confManager.h
******************************************************************************
Copyright 2017 General Electric Company.  All rights reserved.

		Header file for confManager

Date       By                SPR# & Remarks
---------  ----------------  ------------------------------------------------
							Original for magmon4
*****************************************************************************/

#include <string>
#include <unordered_map>
#include <list>

struct section
{
    std::string name;
    std::unordered_map<std::string, std::string> keyvalues;
};

class config
{
public:
    config(const std::string& filename);
    config(void);
    section* get_section(const std::string& sectionname);
    std::list<section>& get_sections();
    void set_value(const std::string& sectionname, const std::string& keyname,
        const std::string& value);
    std::string get_value(const std::string& sectionname, const std::string& keyname);
    std::string get_os_release(const std::string& keyname);

private:
    void writeConfFile(void);
    void parse(const std::string& filename);

    std::list<section> sections;
    std::string mfilename;
    
};

#endif // CONFIG_HPP_
