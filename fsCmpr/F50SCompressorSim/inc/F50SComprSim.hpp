/**
 * @brief 
 * 
 * @file F50SComprSim.h
 * @author Vipin Kumar
 * @date 2018-08-06
 */

#ifndef F50SComprSim_H_
#define F50SComprSim_H_

#include "F50SComprConstants.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include "singleton.hpp"
#include <unistd.h>
#include <thread>
#include <TinyFrame.h>
#include <atomic>
#include <bits/stdc++.h>
#include "effolkronium/random.hpp"
#include <set>
#include "confManager.h"
#include "UARTIFace.hpp"

#define TEMP_MAX 6
#define PRESSURE_MAX 2
#define CCN_SIZE 10
#define STATUS_CODE_SIZE 10
#define MAX_ALARM 49
#define MAX_ALARM_FROM_BEGIN 10
#define MAX_ALARM_FROM_CONT 10
#define FAIL_PRED_MAX 5
#define RESP_BUFF_SIZE 100
#define SOFT_VERSION "1.00"
#define DATASIM_INTERVAL 3
#define CONF_FILE "/mm4data/F50SConfig.cfg"

using namespace std;
using Random = effolkronium::random_static;

/** pointer to unsigned char */
typedef unsigned char* pu8;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Dump a binary frame as hex, dec and ASCII
 */
void dumpFrame(const uint8_t *buff, size_t len);

/**
 * Dump message metadata (not the content)
 *
 * @param msg
 */
void dumpFrameInfo(TF_Msg *msg);
#ifdef __cplusplus
}
#endif

std::vector <std::string> splitData(const std::string &data, char delim);
void TF_WriteImpl(TinyFrame *tf, const uint8_t *buff, uint32_t len);
TF_Result MM4Listener(TinyFrame *tf, TF_Msg *msg);
typedef std::vector <float> vtemp;

typedef struct F50SData {
    float temperature[TEMP_MAX];          //! All Temperature Values (T1 to T6)
    float pressure[PRESSURE_MAX];         //!All Pressure Values (P1 to P2)
    float totalRunTime;
    float adbrUsageTime;
    float adbrRemainderTime;
    float currentCHOprFreq;
    float changeCHOprFreq;
    float serviceTimer;
    float failPredCriterion[FAIL_PRED_MAX];

    char  ccn[CCN_SIZE];                  //! System Structural File Classification
    int   cal;                            //! Abnormal Stop (Alarm OFF) History (10 Cases)
    int   cwa;                            //! Warning History (10 Cases)
    bool  IsCmprSOn;                //! Compressor Turn ON:: (0 - OFF , 1- ON)
    bool  alarmReset;                     //! Alarm Reset
    bool  indCHOprOn;                     //! Individual Cold Head Operation ON
    bool  indCHOprOff;                    //! Individual Cold Head Operation OFF
    bool  powerStatus;
    std::string softVersion;
    std::string currentStatusCode;
    std::string alarmInfoBegin[MAX_ALARM_FROM_BEGIN];
    std::string alarmInfoCont[MAX_ALARM_FROM_CONT];
    F50SComprMode mF50SComprMode;
};

class F50SComprSim {

public:

using Seconds = std::chrono::duration<double, std::chrono::seconds::period>;

TinyFrame *F50SCompr_tf;
	F50SComprSim() : mThread(&F50SComprSim::rxThread, this),
     mDataSimThread(&F50SComprSim::dataSimThread, this){
        Initialization();
    }
	~F50SComprSim() {
        mKillRxThread = true; 
        mThread.join();
        mKillDataSimThread = true;
        mDataSimThread.join();
    }
    std::vector <std::string> sCmprCmdStr = {
        "MTA",
        "MT1", "MT2", "MT3", "MT4", "MT5", "MT6",
        "MPA",
        "MP1", "MP2",
        "CTT", "CAT", "CAR",
        "CSW", "CCN", "CAL", "CWA",
        "OON", "OFF", "ORS", "OCN", "OCF",
        "ICS", "ICH", "CLT", "CSI", 
        "CAS", "CAN",
        "CF1", "CF2", "CF3", "CF4", "CF5"
    } ;

    std::vector <std::string> comprSStatusCodeStr = {
        "00", "10", "20", "30", "40", "50",
        "60", "70", "80", "90", "100", "110"
    } ;

    const std::string comprSAlarmCode[MAX_ALARM] = {
        "A001", "A002", "A003", "A004", "A006", "A007", "A008", "A009",
        "A101", "A102", "A103", "A104", "A106", "A107", "A108", "A109",
        "A116", "A117", "A118", "A119", "A121", "A122", "A123", "A124",
        "A146", "A147", "A148", "A149", "A151", "A152", "A153", "A154",
        "A165", "A601", "A603", "A605", "A300", "A306", "A307", "A308",
        "A309", "A310", "A311", "A314", "A315", "A316", "A317", "A318",
        "A319"
    };
    
    void Initialization(void);

    void handleRxPacket(TinyFrame *tf, TF_Msg * msg);
    void simulateComprSData(void);
    //! Controls
    void startSCmpr(void);
    void stopSCmpr(void);
    void resetSCmpr(void);
    void changeColdHeadFreq(float coldHeadFreq);
    bool getCmprPowerStatus(void);

    //! Data
    void getAllTemp(float *temperature);
    float getHeTempPreH20(void) { return pF50SData.temperature[eCmprSHeTempPreH20]; }
    float getHeTempAftH20(void) { return pF50SData.temperature[eCmprSHeTempAftH20]; }
    float getOilTempPreH20(void) { return pF50SData.temperature[eCmprSOilTempPreH20]; }
    float getOilTempAftH20(void) { return pF50SData.temperature[eCmprSOilTempAftH20]; }
    float getWaterTempInlet(void) { return pF50SData.temperature[eCmprsSWaterTempInlet]; }
    float getWaterTempOutlet(void) { return pF50SData.temperature[eCmprsSWaterTempOutlet]; }
    
    float getSupplyPressure(void) { return pF50SData.pressure[eCmprSHePresSupply]; }
    float getReturnPressure(void) { return pF50SData.pressure[eCmprSHePresReturn]; }
    float getColdHeadFreq(void) { return pF50SData.currentCHOprFreq; }
    std::string getSoftVersion(void);

    void setAllTemp(void);
    void setHeTempPreH20(float temp);
    void setHeTempAftH20(float temp);
    void setOilTempPreH20(float temp);
    void setOilTempAftH20(float temp);
    void setWaterTempInlet(float temp);
    void setWaterTempOutlet(float temp);
    void setAllPres(void);
    void setPresAlarmInfoBegin(void);
    void setPresAlarmInfoCont(void);
    //! STATUS
    F50SComprMode getF50SMode(void);
    std::string getCurrStatusCode(void);
    void setCurrStatusCode(void);

private:
    void rxThread();
    void dataSimThread();
    void respondAllTemp(TinyFrame *tf, TF_Msg * msg);
    void respondHeTempPreH2O(TinyFrame *tf, TF_Msg * msg);
    void respondHeTempAftH2O(TinyFrame *tf, TF_Msg * msg);
    void respondOilTempPreH2O(TinyFrame *tf, TF_Msg * msg);
    void respondOilTempAftH2O(TinyFrame *tf, TF_Msg * msg);
    void respondWaterTempInlet(TinyFrame *tf, TF_Msg * msg);
    void respondWaterTempOutlet(TinyFrame *tf, TF_Msg * msg);
    void respondAllPres(TinyFrame *tf, TF_Msg * msg);
    void respondCurStatusCode(TinyFrame *tf, TF_Msg * msg);
    void respondOnSCmpr(TinyFrame *tf, TF_Msg * msg);
    void respondOffSCmpr(TinyFrame *tf, TF_Msg * msg);
    void respondPresAlarmInfoBegin(TinyFrame *tf, TF_Msg * msg);
    void respondPresAlarmInfoCont(TinyFrame *tf, TF_Msg * msg);
    void respondF50SComprMode(F50SComprMode mode);
    void respondColdHeadFreq(std::vector <std::string> &token, TinyFrame *tf, TF_Msg * msg);
    void respondSoftVersion(TinyFrame *tf, TF_Msg * msg);

    Seconds tickInterval () const {
            return Seconds {DATASIM_INTERVAL};
    }
    
    F50SData pF50SData;
    std::atomic <bool> mKillRxThread = { false };
    std::atomic <bool> mKillDataSimThread = { false };
    std::thread mThread;
    std::thread mDataSimThread;
    DataBuffer recvDataBuff;
    char buff[RESP_BUFF_SIZE];

};

typedef Singleton<F50SComprSim> F50SComprSingleton;


#endif // F50SComprSim_H_
