/**
 * @brief 
 * 
 * @file UARTIFace.hpp
 * @author Vipin Kumar
 * @date 2018-08-23
 */

#ifndef UARTIFace_H_
#define UARTIFace_H_

#include <iostream>
#include <vector>
#include "SerialStream.h"
#include "SerialPort.h"
#include "singleton.hpp"

#define UART_DEV_FILE "/dev/ttyUSB0"
#define READ_TIMEOUT 190 //! In MilliSeconds

using namespace LibSerial ;

class UARTIFace {
    SerialPort serial_port ;
public:
    UARTIFace();
    ~UARTIFace() {
        DeInitUartComm();
    }
    void commInit(void);
    void DeInitUartComm(void);
    void waitUntilDataAvailable(void);
    void readPort(std::vector <uint8_t> &recvbuf);
    void writePort(const uint8_t * wbuff, uint32_t len);

private:

};
typedef Singleton<UARTIFace> UARTIFaceSingleton;



#endif // UARTIFace_H_
