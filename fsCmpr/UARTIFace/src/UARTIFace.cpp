#include "UARTIFace.hpp"
#include <unistd.h>
UARTIFace::UARTIFace() {
    commInit();
}

void UARTIFace::commInit(void) {
   //
    // Open the serial port.
    //
    serial_port.Open( UART_DEV_FILE) ;
    
    //
    // Set the baud rate of the serial port.
    //
    serial_port.SetBaudRate( BaudRate::BAUD_115200  ) ;
    
    //
    // Set the number of data bits.
    //
    serial_port.SetCharacterSize( CharacterSize::CHAR_SIZE_8 ) ;
   
     
    //
    // Disable parity.
    //
    serial_port.SetParity( Parity::PARITY_NONE ) ;
   

    //
    // Turn on hardware flow control.
    //
    serial_port.SetFlowControl( FlowControl::FLOW_CONTROL_NONE ) ;
   
    //
    // Set the number of stop bits.
    //
    serial_port.SetStopBits( StopBits::STOP_BITS_1 ) ;
   
  
    
}

/**
 * @brief DeInitialized the UART Communication
 * 
 */
void UARTIFace::DeInitUartComm(void) {
    if(serial_port.IsOpen())
        serial_port.Close();
}

void UARTIFace::waitUntilDataAvailable(void) {
     // Wait for data to be available at the serial port.
    while(!serial_port.IsDataAvailable()) 
    {
        usleep(1000);
    }
}

 //
    // Do not skip whitespace characters while reading from the
    // serial port.
    //
    // serial_port.unsetf( std::ios_base::skipws ) ;
    //
void UARTIFace::readPort(DataBuffer &readbuf) {
    size_t ms_timeout = READ_TIMEOUT;
    try
    {
        // Read as many bytes as are available during the timeout period.
        serial_port.Read(readbuf, 0, ms_timeout);
    }
    catch (ReadTimeout)
    {
        for (size_t i = 0; i < readbuf.size(); i++)
        {
            std::cout << std::hex << (int)readbuf.at(i) << " " << std::flush;
        }

        std::cerr << "The Read() call timed out waiting for additional data." << std::endl;
    }
}

void UARTIFace::writePort(const uint8_t * wbuff, uint32_t len) {
        DataBuffer writebuf;
    for(auto i = 0; i < len ; i++) {
        writebuf.push_back((unsigned char) wbuff[i]);
    }

    serial_port.Write(writebuf);
    serial_port.DrainWriteBuffer();
    for(auto i = writebuf.begin(); i != writebuf.end(); i++)
        std::cout << std::hex << (int)*i << " ";
    std::cout << std::endl;
}
