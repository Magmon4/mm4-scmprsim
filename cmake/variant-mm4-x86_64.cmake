
set(CMAKE_BUILD_TYPE "RELEASE")

# Note: cmake automatically adds a -DNDEBUG definition due to the RELEASE build type.

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -m64 -Wall -Wextra -pedantic")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -m64 -Wall -Wextra -pedantic")

# Note: cmake automatically adds -rdynamic to CMAKE_*_LINKER_FLAGS

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER gcc)

set(CMAKE_CXX_COMPILER g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
