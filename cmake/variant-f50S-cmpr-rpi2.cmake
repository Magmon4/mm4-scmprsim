set(CMAKE_BUILD_TYPE "RELEASE")
set(SDK_DIR "${PROJECT_SOURCE_DIR}/sdk/f50S-cmpr-rpi2")

# Note: cmake automatically adds a -DNDEBUG definition due to the RELEASE build type.
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS_RELEASE} -std=gnu11 -Wall -Wextra -Wl,-z,now -pedantic -O2 -pipe -fPIE -Wno-variadic-macros --sysroot=${SDK_DIR}/sysroots/cortexa7hf-neon-vfpv4-poky-linux-gnueabi")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS_RELEASE} -std=c++14 -Wall -Wextra -Wl,-z,now -pedantic -O2 -pipe -fPIE -Wno-variadic-macros --sysroot=${SDK_DIR}/sysroots/cortexa7hf-neon-vfpv4-poky-linux-gnueabi")


# Note: cmake automatically adds -rdynamic to CMAKE_*_LINKER_FLAGS

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_SYSROOT ${SDK_DIR}/sysroots/cortexa7hf-neon-vfpv4-poky-linux-gnueabi)

set(CMAKE_C_COMPILER ${SDK_DIR}/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gcc)
set(CMAKE_CXX_COMPILER ${SDK_DIR}/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-g++)


set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)


