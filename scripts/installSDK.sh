#!/bin/bash

set -e
TOP_DIR=".."
MM4_IMX="mm4-imx"
SCMPR_RPI2="f50S-cmpr-rpi2"

VARIANT="$1"
SDK_NAME="$2"
SDK_PATH="$TOP_DIR/sdk"

display_usage()
{
    echo "installSDK.sh script:"
    echo ""
    echo "Usage:"
    echo -e "\tinstallSDK.sh <board variant> <sdk>"
    echo -e "\t<board variant> : mm4-imx | f50S-cmpr-rpi2"
    echo -e "\tFor Eg : ./installSDK.sh mm4-imx ../sdk/fslc-framebuffer-glibc-\
x86_64-core-image-minimal-armv7at2hf-neon-toolchain-2.2.1.sh"
    echo ""
}

install_sdk()
{
    # Change permission
    chmod +x $SDK_NAME
    echo "Installing SDK for $VARIANT ..."
    "$SDK_NAME" -d "$SDK_PATH/$VARIANT" -y
     
    echo "SDK installed sucessfully..."
}

main()
{
    if [ $# != 2 ]; then
        display_usage
        exit
    fi
    
    case $VARIANT in
    $MM4_IMX)
	install_sdk
    ;;
    $SCMPR_RPI2)
	install_sdk
    ;;
    *)
        display_usage
    esac
}

main "$@"
