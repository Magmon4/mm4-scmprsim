#!/bin/bash

#set -x

TOP_DIR="$(pwd)/.." 
MM4_X86_64="mm4-x86_64"
MM4_X86_64_DBG="mm4-x86_64-dbg"
SCMPR_RPI2="f50S-cmpr-rpi2"
SCMPR_RPI2_DBG="f50S-cmpr-rpi2-dbg"

display_usage() {
    echo "Magmon4 build system setup script."
    echo ""
    echo "Usage:"
    echo -e "\tsource <TOP_DIR>/scripts/setup.sh <VARIANT> <BOARD_NAME>"
    echo -e "\t<VARIANT> : \
	  mm4-imx | mm4-imx-dbg \
	| mm4-x86_64 | mm4-x86_64-dbg \
	| f50S-cmpr-rpi2 | f50S-cmpr-rpi2-dbg"
    echo -e "\t<BOARD_NAME> : \
	  elo \
	| desktop \
	| magmon4"
    echo ""
    exit
}

source_sdk_environment() {
    case $VARIANT in
    $SCMPR_RPI2)
        source $TOP_DIR/sdk/f50S-cmpr-rpi2/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
    ;;
    $SCMPR_RPI2_DBG)
        source $TOP_DIR/sdk/f50S-cmpr-rpi2/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
    ;;
    $MM4_X86_64)
    ;;
    $MM4_X86_64_DBG)
    ;;
    *)
        echo "Invalid variant"
   esac
}


check_validity() {
     if [ $# != 2 ]; then
        display_usage
     fi


     if ! cmake --version &>/dev/null ; then
         echo "ERROR: cmake not found."
         exit
     fi

     if [ ! -e $TOP_DIR/scripts/setup.sh ] ; then
         echo "ERROR: This script can't called from this directory."
	 display_usage
         exit
     fi

     if [ ! -e "$TOP_DIR/cmake/variant-$VARIANT.cmake" ] ; then
         echo "ERROR: Unknown variant \"$VARIANT\"."
	 display_usage
         exit
     fi
}

main() {
    VARIANT="$1"
    BOARD_NAME="$2"
    check_validity $VARIANT $BOARD_NAME
    source_sdk_environment
    if [ ! -d "$TOP_DIR/build/$VARIANT" ] ; then
        mkdir -p "$TOP_DIR/build/$VARIANT"
    fi
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo "Build Environment has been configured for $BOARD_NAME."
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
}

main "$@"

