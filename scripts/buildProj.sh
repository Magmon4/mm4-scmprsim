#!/bin/bash

set -e

TOP_DIR="$(pwd)/.."
SDK_DIR="$TOP_DIR/sdk"
VARIANT=$1
BOARD_NAME=$2
MM4_X86_64="mm4-x86_64"
MM4_X86_64_DBG="mm4-x86_64-dbg"
SCMPR_RPI2="f50S-cmpr-rpi2"
SCMPR_RPI2_DBG="f50S-cmpr-rpi2-dbg"
BOARD_RPI2="rpi2"
DESKTOP="desktop"

display_help() {
    echo "buildMM4 script:"
    echo ""
    echo "Usage:"
    echo -e "\tbuildProj.sh <VARIANT> <BOARD_NAME>"
    echo -e "\t<VARIANT> : $MM4_X86_64 | $MM4_X86_64_DBG\
	$SCMPR_RPI2 | $SCMPR_RPI2_DBG"
    echo -e "\t<BOARD_NAME> : $BOARD_ELO | $DESKTOP | $BOARD_RPI2"
    echo ""
    exit
}

compileMM4() {
    
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo "     MM4 BUILD STARTED !!!  BUILD_PARAM = $VARIANT  $BOARD_NAME      "  
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" 
    
    
    source $TOP_DIR/scripts/setup.sh "$VARIANT" "$BOARD_NAME"
    pushd "$TOP_DIR/build/$VARIANT"
      echo $SDKTARGETSYSROOT
      cmake $TOP_DIR "-DVARIANT=$VARIANT" "-DBOARD_NAME=$BOARD_NAME"
      cmake --version
      pwd
      make help
      echo $SDKTARGETSYSROOT
      make all install   
      echo $SDKTARGETSYSROOT
    popd
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo "     MM4 BUILD SUCCESSFULLY !!!    "
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
}


main() {
    if [ $# != 2 ]; then
        display_help
    fi

    if [ $1 == $MM4_X86_64 ] || [ $1 == $MM4_X86_64_DBG ] \
	|| [ $1 == $SCMPR_RPI2 ] || [ $1 == $SCMPR_RPI2_DBG ] \
	&& [ $2 == $DESKTOP ] \
	|| [ $2 == $BOARD_RPI2 ];then
        compileMM4
    else
       display_help
    fi
}

main "$@"
